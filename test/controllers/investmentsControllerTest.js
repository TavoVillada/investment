const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const InvestmentsRepository = require('../../app/Repositories/investmentsRepository');
const Helper = require('../Helper');
const app = require('../../index');

const API = '/investments';

chai.use(chaiHttp);

describe('Endpoints Investments ', () => {
  before(async () => {
    Helper.migrate();
  });

  after(async () => {
    await Helper.clear();
  });

  it('first test', () => chai
    .request(app)
    .post(API)
    .send({
      idUser: 4, idProject: 4, status: true, idRewards: 4,
    })
    .then(async (res) => {
      const { body } = res;
      console.log(body);
      assert.ok(true);
    })
    .catch(() => {
      assert.fail();
    }));

  it('empty test', () => chai
    .request(app)
    .post(API)
    .send({})
    .then((error) => {
      assert.equal(error.status, 400);
      console.log(error.status);
    })
    .catch((error) => {
      assert.equal(error.status, 400);
    }));

  it('second test', () => chai
    .request(app)
    .get('/investments/project/4')
    .then(async (res) => {
      const investment = await InvestmentsRepository.getInvestmentByIdProject(4);
      const { body } = res;
      console.log(body, investment);
      assert.equal(body.idProject, investment.idProject);
    })
    .catch((error) => {
      assert.equal(error.status, 400);
      console.log(error.status);
    }));


  it('third test', () => chai
    .request(app)
    .get('/investments/user/4')
    .then(async (res) => {
      const investment = await InvestmentsRepository.getInvestmentByIdUser(4);
      const { body } = res;
      console.log(body, investment);
      assert.equal(body.idUser, investment.idUser);
    })
    .catch((error) => {
      assert.equal(error.status, 400);
      console.log(error.status);
    }));
});
