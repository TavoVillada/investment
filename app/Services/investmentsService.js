const InvestmentRepository = require('../Repositories/investmentsRepository');
// const log4js = require('../Utils/Logger');

// const defaultLogger = log4js.getLogger('InvestmentService');

class InvestmentService {
  async getInvestmentsByIdProject(id) {
    // const { logger = defaultLogger } = options;
    // logger.info(`Start InvestmentsService.getInvestmentsByIdProject: params ${JSON.stringify(id)}`);

    return InvestmentRepository.getInvestmentByIdProject(id);
  }

  async getInvestmentByIdUser(id) {
    // const { logger = defaultLogger } = options;

    // logger.info(`Start InvestmentsService.getInvestmentsByIdUser: params ${JSON.stringify(id)}`);

    return InvestmentRepository.getInvestmentByIdUser(id);
  }

  async createInvestment(investment) {
    // const { logger = defaultLogger } = options;
    // logger.info(`Start InvestmentService.createInvestment: params ${JSON.stringify(investment)}`);
    const [res] = await InvestmentRepository.createInvestment(investment);

    return res;
  }
}

const investmentService = new InvestmentService();
module.exports = investmentService;
