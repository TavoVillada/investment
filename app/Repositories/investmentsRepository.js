const DB = require('../Utils/DataBase');

class InvestmentsRepositories {
  constructor() {
    this.createInvestment = async (investments) => DB('investments').insert(investments).returning('*');
    this.getInvestmentByIdProject = async (idProject) => DB('investments').where({ idProject }).select('*').first();
    this.getInvestmentByIdUser = async (idUser) => DB('investments').where({ idUser }).select('*').first();
  }
}

const investmentsRepository = new InvestmentsRepositories();
module.exports = investmentsRepository;
